﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.Http;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Text;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;



// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace IoT_Weather
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //A class which wraps the barometric sensor
        BME280 BME280;
        DispatcherTimer dispatcherTimer;
        DateTimeOffset startTime;
        DateTimeOffset lastTime;
        DateTimeOffset stopTime;
        int timesTicked = 1;
        int timesToTick = 161280; // Month, although the device should reboot daily at 0:00
        static DeviceClient deviceClient;
        static string iotHubUri = "";
        static string deviceId = "";
        static string deviceKey = "";

        public async void DispatcherTimerSetup()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 15);
            //IsEnabled defaults to false
            Debug.WriteLine("dispatcherTimer.IsEnabled = " + dispatcherTimer.IsEnabled + "\n");
            startTime = DateTimeOffset.Now;
            lastTime = startTime;
            Debug.WriteLine("Calling dispatcherTimer.Start()\n");
            //Create a new object for our barometric sensor class
            BME280 = new BME280();
            //Initialize the sensor
            await BME280.Initialize();

            Debug.WriteLine("Initializing cloud device...");
            // Original
            deviceClient = DeviceClient.Create(iotHubUri,AuthenticationMethodFactory.CreateAuthenticationWithRegistrySymmetricKey(deviceId, deviceKey),TransportType.Http1);
            // Second attempt
            //deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey(deviceId, deviceKey));
            // Local file attempt
            //deviceClient = DeviceClient.CreateFromConnectionString(iotHubUri, TransportType.Http1);
            Debug.WriteLine("complete\n");

            dispatcherTimer.Start();
            //IsEnabled should now be true after calling start
            Debug.WriteLine("dispatcherTimer.IsEnabled = " + dispatcherTimer.IsEnabled + "\n");
        }

        async void dispatcherTimer_Tick(object sender, object e)
        {
            DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - lastTime;
            lastTime = time;
            //Time since last tick should be very very close to Interval
            Debug.WriteLine(timesTicked + "\t time since last tick: " + span.ToString() + "\n");
            timesTicked++;
            try
            {

                //Create a constant for pressure at sea level. 
                //This is based on your local sea level pressure (Unit: Hectopascal)

                //const float seaLevelPressure = 1013.25f;

                //while (true)
                //{
                    Reading singleReading = new Reading();
                    singleReading.tick = timesTicked;
                    singleReading.timestamp = DateTime.Now;
                    singleReading.temperature = await BME280.ReadTemperature();
                    // Convert temp to F and adjust, (minus 3.64 degrees due to testing -DM)
                    singleReading.temperature = (singleReading.temperature * 9 / 5) + 32 - 3.64F;
                    singleReading.pressure = await BME280.ReadPreasure();
                    singleReading.humidity = await BME280.ReadHumidity();
                //Debug.WriteLine(singleReading.tick + "," + singleReading.timestamp + "," + singleReading.temperature + "," + singleReading.pressure + "," + singleReading.humidity);
                var jsonPayload = JsonConvert.SerializeObject(singleReading);
                var message = new Message(Encoding.ASCII.GetBytes(jsonPayload));
                await deviceClient.SendEventAsync(message);
                Debug.WriteLine("{0} > Sending message: {1}", DateTime.Now, jsonPayload);
                //System.Threading.Tasks.Task.Delay(5000).Wait();
                //}
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            if (timesTicked > timesToTick)
            {
                stopTime = time;
                Debug.WriteLine("Calling dispatcherTimer.Stop()\n");
                dispatcherTimer.Stop();
                //IsEnabled should now be false after calling stop
                Debug.WriteLine("dispatcherTimer.IsEnabled = " + dispatcherTimer.IsEnabled + "\n");
                span = stopTime - startTime;
                Debug.WriteLine("Total Time Start-Stop: " + span.ToString() + "\n");
                //System.Diagnostics.Process.Start("shutdown -r -t 0");
            }
        }

        public MainPage()
        {
            this.InitializeComponent();
        }
        //This method will be called by the application framework when the page is first loaded
        protected override void OnNavigatedTo(NavigationEventArgs navArgs)
        {
            Debug.WriteLine("MainPage::OnNavigatedTo");
            DispatcherTimerSetup();

            // Not yet
            //MakePinWebAPICall();
        }

        public class Reading
        {
            public Int32 tick;
            public DateTime timestamp;
            public float temperature;
            public float pressure;
            public float humidity;
        }

        /// <summary>
        // This method will put your pin on the world map of makers using this lesson.
        // This uses imprecise location (for example, a location derived from your IP 
        // address with less precision such as at a city or postal code level). 
        // No personal information is stored.  It simply
        // collects the total count and other aggregate information.
        // http://www.microsoft.com/en-us/privacystatement/default.aspx
        // Comment out the line below to opt-out
        /// </summary>
        public void MakePinWebAPICall()
        {
            try
            {
                HttpClient client = new HttpClient();

                // Comment this line to opt out of the pin map.
                client.GetStringAsync("http://adafruitsample.azurewebsites.net/api?Lesson=203");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Web call failed: " + e.Message);
            }
        }
    }
}
